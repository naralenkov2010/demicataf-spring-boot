package com.demica.taf.helper;

import com.demica.taf.context.bo.Portfolio;
import com.demica.taf.utils.FileUtilities;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.apache.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

@Component
public class InvoiceHelper {

    private final String TEST_PORTFOLIO_PATH = "src/test/resources/portfolioJson";
    private final Logger logger = Logger.getLogger(InvoiceHelper.class);

    public List<Portfolio> readTestPortfolios() {
        List<Portfolio> portfolios = new ArrayList<>();
        JsonReader reader;
        File dir = new File(TEST_PORTFOLIO_PATH);
        List<File> fileList = FileUtilities.getDirectoryFiles(dir.toPath());
        for (File file: fileList) {
            logger.debug("Reading portfolio from file: " + file.toString());
            try {
                reader = new JsonReader(new FileReader(file));
                portfolios.add(new Gson().fromJson(reader, Portfolio.class));
            } catch (FileNotFoundException e) {
                logger.error("File " + file.toString() + " not found!");
            }
        }
        return portfolios;
    }
}
