package com.demica.taf.helper;

import com.demica.taf.connect.mq.ActiveMQ;
import com.demica.taf.context.bo.Portfolio;
import com.google.gson.Gson;
import com.google.gson.stream.JsonReader;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import javax.jms.*;
import java.io.StringReader;
import java.math.BigInteger;
import java.util.concurrent.TimeUnit;

import static com.jayway.awaitility.Awaitility.await;

@Component
public class ActiveMqHelper {

  private final Logger logger = Logger.getLogger(ActiveMqHelper.class);
  private static final String PORTFOLIO_MFST = "portfolioMfst";

  @Value("${queue.all-invoices}")
  private String allInvoicesQueue;

  @Value("${queue.unapproved-invoices}")
  private String unapprovedInvoicesQueue;

  @Value("${queue.approved-invoices}")
  private String approvedInvoicesQueue;

  @Value("${queue.funded-invoices}")
  private String fundedInvoicesQueue;

  @Value("${queue.processed-invoices}")
  private String processedInvoicesQueue;

  private Destination destinationUnapproved;
  private Destination destinationFunded;
  private Destination destinationAll;
  private Destination destinationApproved;
  private Destination destinationProcessed;

  @Autowired
  private ActiveMQ activeMQ;

  @PostConstruct
  public void init() {
    try {
      destinationAll = activeMQ.getActiveMqSession().createQueue(allInvoicesQueue);
      logger.debug("Create destination: " + allInvoicesQueue);
      destinationUnapproved = activeMQ.getActiveMqSession().createQueue(unapprovedInvoicesQueue);
      logger.debug("Create destination: " + unapprovedInvoicesQueue);
      destinationApproved = activeMQ.getActiveMqSession().createQueue(approvedInvoicesQueue);
      logger.debug("Create destination: " + approvedInvoicesQueue);
      destinationFunded = activeMQ.getActiveMqSession().createQueue(fundedInvoicesQueue);
      logger.debug("Create destination: " + fundedInvoicesQueue);
      destinationProcessed = activeMQ.getActiveMqSession().createQueue(processedInvoicesQueue);
      logger.debug("Create destination: " + processedInvoicesQueue);
    } catch (JMSException e) {
      logger.error("Error while creating destination!", e);
    }
  }

  public Destination getDestinationProcessed() {
    return destinationProcessed;
  }

  public Destination getDestinationUnapproved() {
    return destinationUnapproved;
  }

  public Destination getDestinationFunded() {
    return destinationFunded;
  }

  public Destination getDestinationAll() {
    return destinationAll;
  }

  public Destination getDestinationApproved() {
    return destinationApproved;
  }

  public TextMessage createTextMessage(Portfolio portfolio) {
    TextMessage inputMessage = null;
    String messageBody = new Gson().toJson(portfolio);
    inputMessage = activeMQ.createTextMessage(messageBody);
    logger.debug("Message created!");
    return inputMessage;
  }

  public Message waitMessage(MessageConsumer jmsConsumer)
      throws JMSException {
    int counter = 0;
    Message message;
    while (true) {
      message = jmsConsumer.receive(5000);
      if (message != null) {
        logger.info("We got message with id =" + message.getJMSMessageID());
        return message;
      } else if (counter > 250) {
        logger.error("The message doesn't appear in queue!!!");
      }
      counter++;
    }
  }

  public boolean waitPortfolioMessage(MessageConsumer jmsConsumer, Portfolio portfolio) {
    try {
      await().pollInterval(1, TimeUnit.SECONDS)
          .atMost(60, TimeUnit.SECONDS).catchUncaughtExceptions()
          .until(() -> isPortfolioInMessage(portfolio, (TextMessage) waitMessage(jmsConsumer)));
      return true;
    } catch (Exception e) {
      logger.error(String.format("Error while waiting message in the queue for portfolio with manifest = %s.", portfolio.portfolioMfst));
      return false;
    }
  }

  private boolean isPortfolioInMessage(Portfolio portfolio, TextMessage message) {
    BigInteger currentPortfolioMfst = getPortfolioManifest(portfolio, message);
    return currentPortfolioMfst == null ? false : currentPortfolioMfst.equals(portfolio.portfolioMfst);
  }

  private BigInteger getPortfolioManifest(Portfolio portfolio, TextMessage message) {
    String jsonMessage;
    try {
      jsonMessage = message.getText();
      JsonReader jsonReader = new JsonReader(new StringReader(jsonMessage));
      jsonReader.beginObject();
      while (jsonReader.hasNext()) {
        String fieldName = jsonReader.nextName();
        if (fieldName.equals(PORTFOLIO_MFST)) {
          return new BigInteger(jsonReader.nextString());
        } else {
          jsonReader.skipValue();
        }
      }
    } catch (Exception e) {
      logger.error("Error while checking portfolio with mfst - " + portfolio.portfolioMfst);
    }
    return null;
  }
}
