package com.demica.taf.helper;

import com.demica.taf.connect.db.Database;
import com.demica.taf.context.bo.Invoice;
import com.demica.taf.context.bo.Portfolio;
import java.math.BigDecimal;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class DbHelper {
  @Autowired
  Database database;

  private final Logger logger = Logger.getLogger(DbHelper.class);

  private final String DELETE_TEST_INVOICES_STR_QUERY = "DELETE FROM str WHERE mfst = ?";
  private final String DELETE_TEST_INVOICES_STR_HDR_QUERY = "DELETE FROM str_hdr WHERE mfst = ?";
  private final String GET_CURRENT_SEQ_NUM_QUERY = "SELECT crnt_seq_num from prtflio WHERE mfst = ?";
  private final String GET_PORTFOLIO_STATUS_QUERY = "select prtflio_stat from prtflio where mfst = ?";
  private final String GET_INVOICE_LEVEL_QUERY = "select crnt_lvl from str_hdr where mfst = ?";
  private final String SET_STR_HDR_IDENTITY_INSERT_ON_QUERY = "set identity_insert str_hdr on";
  private final String SET_STR_HDR_IDENTITY_INSERT_OFF_QUERY = "set identity_insert str_hdr off";
  private final String INSERT_TEST_INVOICE_STR_QUERY = "INSERT INTO str (mfst, chng_by, date_mod, vsn, audit, arch, stat, ownr_lbl, is_prchsd, " +
      "inv_type, inv_stat, inv_deflt_stat, otc_mfst, prtflio_mfst, seq_num, prev_seq_num, actn, lvl, curr_mfst, amnt, maty_date, issue_date, " +
      "posting_date, rate_date, is_elig, elig_flag, enty_prog_mfst, src_enty_prog_mfst, enty_mfst, trust_mfst, trust_code, cntrprty_mfst, " +
      "cntrprty_code, cntrprty_name, prchs_price, prchs_rate, po_box, addr_line1, addr_line2, addr_line3, addr_line4, cntry_mfst, cntry_code, " +
      "state_mfst, state_code, city_mfst, city_code, post_code, data_src_mfst, outstdng_prin, xpctd_pay_date, dltion_amnt, same_prd_dltion_amnt, " +
      "latr_prd_dltion_amnt, buy_back_stat, lim_amnt, date_from, date_to, event_date, deal_mfst, pndng_actn, usr_def_1, usr_def_2, usr_def_3, " +
      "usr_def_4, usr_def_5, usr_def_6, usr_def_7, usr_def_8, usr_def_9, usr_def_10, usr_def_11, usr_def_12, usr_def_13, usr_def_14, usr_def_15, " +
      "usr_def_16, usr_def_17, usr_def_18, usr_def_19, usr_def_20, usr_def_21, usr_def_22, usr_def_23, usr_def_24, usr_def_25, usr_def_26, usr_def_27, " +
      "usr_def_28, usr_def_29, usr_def_30, usr_def_1_mfst, usr_def_2_mfst, usr_def_3_mfst, usr_def_4_mfst, usr_def_5_mfst, usr_def_6_mfst, " +
      "usr_def_7_mfst, usr_def_8_mfst, usr_def_9_mfst, usr_def_10_mfst, usr_def_11_mfst, usr_def_12_mfst, usr_def_13_mfst, usr_def_14_mfst, " +
      "usr_def_15_mfst, usr_def_16_mfst, usr_def_17_mfst, usr_def_18_mfst, usr_def_19_mfst, usr_def_20_mfst, usr_def_21_mfst, usr_def_22_mfst, " +
      "usr_def_23_mfst, usr_def_24_mfst, usr_def_25_mfst, usr_def_26_mfst, usr_def_27_mfst, usr_def_28_mfst, usr_def_29_mfst, usr_def_30_mfst, " +
      "PWDD_amnt, SSDD_amnt, CID_amnt, GCD_amnt, FCD_amnt, LCD_amnt, LFD_amnt) " +
      "VALUES(?, 8, getdate(), 1, 0, 0, 1, 999001158, 0, 1, 1, 0, NULL, 150002007692548, ?, ?, NULL, ?, 10090, ?, '2019-04-13 00:00:00.000', " +
      "'2019-04-12 00:00:00.000', '2019-05-23 00:00:00.000', NULL, 1, 0, NULL, NULL, 150002007692528, NULL, NULL, 150002007692528, 'test1', NULL, " +
      "0.0000, 0.0000000000, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 150002007692450, ?, '2019-05-24 00:00:00.000', " +
      "0.0000, 0.0000, NULL, 0, 0.0000, '2019-05-23 10:27:20.000', '5000-01-01 00:00:00.000', '2019-05-23 10:27:20.000', NULL, 0, 'test', '0', '1', " +
      "NULL, NULL, '1', '0', '1', '1', NULL, ?, NULL, 'test', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
      "NULL, NULL, NULL, 150002007692447, NULL, NULL, NULL, NULL, 11933, 11936, 11941, 11945, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, " +
      "NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000, 0.0000)";

  private static final String INSERT_TEST_INVOICE_STR_HDR_QUERY = "INSERT INTO str_hdr (mfst, chng_by, date_mod, vsn, audit, arch, stat, ownr_lbl, " +
      "is_prchsd, inv_id, inv_stat, inv_type, crnt_seq_num, inpt_file_fmt_mfst, outstdng_prin, xpctd_pay_date, orig_amnt, orig_seq_num, crnt_lvl, inv_stat_chng) " +
      "VALUES(?, 8, getdate(), 1, 0, 0, 1, 999001158, 0, ?, 1, NULL, ?, 150002007692549, ?, '2019-05-24 00:00:00.000', ?, ?, ?, NULL)";



  public void deleteTestInvoices(List<Portfolio> portfolioList) {
    try (Connection connection = database.getConnection();
         PreparedStatement psDeleteStr = connection.prepareStatement(DELETE_TEST_INVOICES_STR_QUERY);
         PreparedStatement psDeleteStrHdr = connection.prepareStatement(DELETE_TEST_INVOICES_STR_HDR_QUERY)) {
      for (Portfolio portfolio: portfolioList) {
        for (Invoice invoice: portfolio.invoices){
          psDeleteStr.setBigDecimal(1, new BigDecimal(invoice.invoiceMfst));
          psDeleteStr.execute();
          commitConnection(connection);

          psDeleteStrHdr.setBigDecimal(1, new BigDecimal(invoice.invoiceMfst));
          psDeleteStrHdr.execute();
          commitConnection(connection);
        }
      }
    }catch (SQLException e){
      logger.error("Error while deleting test invoices!");
    }
  }

  public int getTestPortfolioStatus(Portfolio portfolio) throws SQLException {
    ResultSet resultSet = null;
    try (PreparedStatement psGetPortfolioStr = database.getConnection().prepareStatement(GET_PORTFOLIO_STATUS_QUERY)) {
      psGetPortfolioStr.setBigDecimal(1, new BigDecimal(portfolio.portfolioMfst));
      resultSet = psGetPortfolioStr.executeQuery();
      if (resultSet.next()) {
        return resultSet.getInt("prtflio_stat");
      }
    }
    return -1;
  }

  public int getTestInvoiceLevel(Invoice invoice) throws SQLException {
    ResultSet resultSet = null;
    try (PreparedStatement psGetPortfolioStr = database.getConnection().prepareStatement(GET_INVOICE_LEVEL_QUERY)) {
      psGetPortfolioStr.setLong(1, invoice.invoiceMfst);
      resultSet = psGetPortfolioStr.executeQuery();
      if (resultSet.next()) {
        return resultSet.getInt("crnt_lvl");
      }
    }
    return -1;
  }

  public void insertTestInvoicesIntoDb(List<Portfolio> portfolios) throws SQLException {
    int counter=0;
    try (Connection connection = database.getConnection();
         PreparedStatement psCurrentSeqNum = connection.prepareStatement(GET_CURRENT_SEQ_NUM_QUERY);
         PreparedStatement psInsertStr = connection.prepareStatement(INSERT_TEST_INVOICE_STR_QUERY);
         PreparedStatement psInsertStrHdr = connection.prepareStatement(INSERT_TEST_INVOICE_STR_HDR_QUERY)) {
      for (Portfolio portfolio : portfolios) {
        int currentSeqNum = getCurrentSeqNum(psCurrentSeqNum, portfolio);
        for (Invoice invoice : portfolio.invoices) {
          insertStr(psInsertStr, connection, invoice.invoiceMfst ,currentSeqNum, invoice.invoiceAmount, "IV0test0000" + counter, 0);

          executeQuery(connection, SET_STR_HDR_IDENTITY_INSERT_ON_QUERY);
          insertStrHdr(psInsertStrHdr, connection, invoice.invoiceMfst, "IV0test0000" + counter, currentSeqNum, invoice.invoiceAmount, 0);
          executeQuery(connection, SET_STR_HDR_IDENTITY_INSERT_OFF_QUERY);
        }
      }

    }
  }

  private void insertStr(PreparedStatement psInsertStr, Connection connection, int mfst, int currentSeqNum, BigDecimal amount, String payableId, int level) throws SQLException {
    psInsertStr.setBigDecimal(1, new BigDecimal(mfst));
    psInsertStr.setInt(2, currentSeqNum);
    psInsertStr.setInt(3, currentSeqNum > 1 ? currentSeqNum - 1 : 0);
    psInsertStr.setBigDecimal(4, new BigDecimal(level));
    psInsertStr.setBigDecimal(5, amount);
    psInsertStr.setBigDecimal(6, amount);
    psInsertStr.setString(7, payableId);

    psInsertStr.execute();
    commitConnection(connection);
  }

  private void insertStrHdr(PreparedStatement psInsertStrHdr, Connection connection, int mfst, String invoiceId, int currentSeqNum, BigDecimal amount, int level) throws SQLException {
    psInsertStrHdr.setBigDecimal(1, new BigDecimal(mfst));
    psInsertStrHdr.setString(2, invoiceId);
    psInsertStrHdr.setInt(3, currentSeqNum);
    psInsertStrHdr.setBigDecimal(4, amount);
    psInsertStrHdr.setBigDecimal(5, amount);
    psInsertStrHdr.setInt(6, currentSeqNum > 1 ? currentSeqNum - 1 : 0);
    psInsertStrHdr.setBigDecimal(7, new BigDecimal(level));

    psInsertStrHdr.execute();
    commitConnection(connection);
  }

  private int getCurrentSeqNum(PreparedStatement psCurrentSeqNum, Portfolio portfolio) throws SQLException {
    psCurrentSeqNum.setBigDecimal(1, new BigDecimal(portfolio.portfolioMfst));
    psCurrentSeqNum.execute();
    try (ResultSet seqNum = psCurrentSeqNum.getResultSet()) {
      if (seqNum.next()) {
        int currSeqNum = seqNum.getInt(1);
        return currSeqNum > 1 ? currSeqNum - 1 : currSeqNum;
      }
      return 0;
    }
  }

  public void executeQuery(Connection connection, String query) throws SQLException {
    try ( PreparedStatement preparedStatement =  connection.prepareStatement(query)) {
      preparedStatement.execute();
      commitConnection(connection);
    }
  }

  private void commitConnection(Connection connection) throws SQLException {
    try {
      connection.commit();
    } catch (SQLException e) {
      connection.rollback();
      throw e;
    }
  }


}
