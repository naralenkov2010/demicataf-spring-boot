package com.demica.taf.connect.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import org.apache.commons.lang3.StringUtils;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class Database {

  @Value("${spring.datasource.url}")
  private String url;

  @Value("${spring.datasource.username}")
  private String userName;

  @Value("${spring.datasource.password}")
  private String password;

  private Connection connection;
  private final Logger logger = Logger.getLogger(Database.class);

  public Connection getConnection() throws SQLException{
    if (connection==null){
      Connection connection = DriverManager.getConnection(url, userName, password);
      connection.setAutoCommit(false);
      return connection;
    } else return connection;
  }

  public Database() {
  }

  public ResultSet executeQuery(String query) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    try {
      validateConnection();
      validateQuery(query);
      stmt = connection
          .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
      logger.debug("[Database] Executing query: \n" + query);
      rs = stmt.executeQuery(query);
    } finally {
      try {
        closeConnection();
      } catch (Exception e) {
        logger.error("Can't close db connection");
      }
    }
    return rs;
  }

  public void executeUpdateQuery(String query) throws SQLException {
    Statement stmt = null;
    ResultSet rs = null;
    try {
      validateConnection();
      validateQuery(query);
      stmt = connection
          .createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
      logger.debug("[Database] Executing query: \n" + query);
      stmt.execute(query);
    } finally {
      try {
        closeConnection();
      } catch (Exception e) {
        logger.error("Can't close db connection");
      }
    }
  }

  private void validateConnection() throws SQLException {
    if (connection == null || connection.isClosed()) {
      String errMsg = "[Database] Connection to database has not been established.";
      logger.error(errMsg);
      throw new RuntimeException(errMsg);
    }
  }

  private void validateQuery(String query) {
    if (StringUtils.isBlank(query)) {
      String errMsg = "[Database] Query can't be EMPTY!";
      logger.error(errMsg);
      throw new RuntimeException(errMsg);
    }
  }

  private void closeConnection() throws SQLException {
    if (connection != null) {
      connection.close();
    }
  }
}