package com.demica.taf.connect.mq;

import javax.annotation.PostConstruct;
import javax.jms.Connection;
import javax.jms.JMSException;
import javax.jms.Session;
import javax.jms.TextMessage;
import org.apache.activemq.ActiveMQConnectionFactory;
import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

@Component
public class ActiveMQ {

  @Value("${spring.activemq.broker-url}")
  private String url;

  @Value("${spring.activemq.user}")
  private String userName;

  @Value("${spring.activemq.password}")
  private String password;

  private ActiveMQConnectionFactory connectionFactory;
  private Connection activeMqConnection;
  private Session activeMqSession;

  private final Logger logger = Logger.getLogger(ActiveMQ.class);

  public ActiveMQ() {
  }

  @PostConstruct
  public void init() {
    try {
      this.connectionFactory = new ActiveMQConnectionFactory(url);
      this.connectionFactory.setUserName(userName);
      this.connectionFactory.setPassword(password);
      this.activeMqConnection = connectionFactory.createConnection();
      this.activeMqSession = this.activeMqConnection.createSession(false, Session.AUTO_ACKNOWLEDGE);
      this.activeMqConnection.start();
    } catch (JMSException e) {
      logger.error("JMS ERROR!: ", e.fillInStackTrace());
    }
  }

  public Session getActiveMqSession() {
    return activeMqSession;
  }

  public void close() {
    try {
      this.activeMqSession.close();
      this.activeMqConnection.close();
    } catch (JMSException e) {
      e.printStackTrace();
    }
  }

  public TextMessage createTextMessage(String jsonString) {
    try {
      return this.activeMqSession.createTextMessage(jsonString);
    } catch (JMSException e) {
      e.printStackTrace();
    }
    return null;
  }
}
