package com.demica.taf.utils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.junit.Assert;

public class FileUtilities extends Assert {

  private static final Logger logger = Logger.getLogger(FileUtilities.class);

  public static void verifyFileDownloaded(File file) {
    File dir = new File(file.getParent());
    File[] dirContents = dir.listFiles();
    boolean flag = false;
    for (int i = 0; i < dirContents.length; i++) {
      if (dirContents[i].getName().equals(file.getName())) {
        dirContents[i].delete();
        flag = true;
        break;
      }
    }
    assertTrue(flag);
  }

  public static boolean isFileExistInDirectoryByMask(Path path, String fileMask) {
    String[] nameAndExt = fileMask.split("\\*");
    String fileNameMask = nameAndExt[0];
    String extension = nameAndExt[1];
    for (File file : getDirectoryFiles(path)) {
      if (file.toString().contains(fileNameMask) && file.toString().endsWith(extension)) {
        logger.info("File " + file.toString() + " was found by mask " + fileMask);
        file.delete();
        return true;
      }
    }
    logger.info("File with mask " + fileMask + " doesn't exist in " + path.toString());
    return false;
  }

  public static List<File> getDirectoryFiles(Path path) {
    List<File> filesInFolder = new ArrayList<>();
    try {
      filesInFolder = Files.walk(path)
          .filter(Files::isRegularFile)
          .map(Path::toFile)
          .collect(Collectors.toList());
    } catch (IOException e) {
      e.printStackTrace();
    }
    return filesInFolder;
  }
}
