package com.demica.taf.context.enums;

public enum InvoiceLevel {
  NOTHING(0), ALL(1), UNAPPROVED(2), APPROVED(3), FUNDED(4);

  public int getValue() {
    return intValue;
  }

  private int intValue;

  InvoiceLevel(int i) {
    this.intValue = i;
  }
}
