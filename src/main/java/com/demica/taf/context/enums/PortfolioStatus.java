package com.demica.taf.context.enums;

public enum PortfolioStatus {
  READY(1),
  PE_READY(10),
  ALL_PROCESSING(20),
  UNAPPROVED_PROCESSING(30),
  APPROVED_PROCESSING(40),
  FUNDED_PROCESSING(50);

  public int getValue() {
    return intValue;
  }

  private int intValue;

  PortfolioStatus(int i) {
    this.intValue = i;
  }
}
