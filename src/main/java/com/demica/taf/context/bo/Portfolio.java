package com.demica.taf.context.bo;

import java.math.BigInteger;
import java.util.List;

public class Portfolio {

  public BigInteger portfolioMfst;
  public List<Integer> limitCurrency;
  public int buyerLimitMfst;
  public int supplierLimitMfst;
  public long supplierMfst;
  public int portfolioCurrency;
  public int portfolioSequenceNumber;
  public List<Invoice> invoices;

}
