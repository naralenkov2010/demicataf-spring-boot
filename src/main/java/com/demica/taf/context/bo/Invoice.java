package com.demica.taf.context.bo;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class Invoice {

  public int invoiceMfst;
  public Eligibility eligibility;
  public BigDecimal invoiceAmount;
  public BigDecimal dilutionAmount;
  public String expectedPayDate;
  public String maturityDate;
  public String issueDate;
  public Long fileFormatMfst;
  public int currProgramme;

  public void setExpectedPayDate(Date expectedPayDate) {
    this.expectedPayDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.UK).format(expectedPayDate);
  }

  public void setMaturityDate(Date maturityDate) {
    this.maturityDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.UK).format(maturityDate);
  }

  public void setIssueDate(Date issueDate) {
    this.issueDate = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss a", Locale.UK).format(issueDate);
  }

  public void setEligibility(Eligibility eligibility) {
    this.eligibility = eligibility;
  }
}
