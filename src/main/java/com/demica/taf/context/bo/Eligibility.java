package com.demica.taf.context.bo;

public class Eligibility {
  public boolean all;
  public boolean unapproved;
  public boolean approved;
  public boolean funded;

  public Eligibility(boolean all, boolean unapproved, boolean approved, boolean funded) {
    this.all = all;
    this.unapproved = unapproved;
    this.approved = approved;
    this.funded = funded;
  }
}
