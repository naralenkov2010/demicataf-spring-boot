package com.demica.taf;

import com.demica.taf.connect.mq.ActiveMQ;
import com.demica.taf.context.bo.Portfolio;
import com.demica.taf.context.enums.InvoiceLevel;
import com.demica.taf.context.enums.PortfolioStatus;
import com.demica.taf.helper.ActiveMqHelper;
import com.demica.taf.helper.DbHelper;
import com.demica.taf.helper.InvoiceHelper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import javax.jms.DeliveryMode;
import javax.jms.MessageConsumer;
import javax.jms.MessageProducer;
import javax.jms.TextMessage;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {AppConfig.class})
@Transactional
public class RunTests {

  @Autowired
  ActiveMQ activeMQ;
  @Autowired
  ActiveMqHelper activeMqHelper;
  @Autowired
  InvoiceHelper invoiceHelper;
  @Autowired
  DbHelper dbHelper;

  List<Portfolio> portfolioList = new ArrayList<>();

  @Before
  public void before() {
    try {
      portfolioList.addAll(invoiceHelper.readTestPortfolios());
      dbHelper.deleteTestInvoices(portfolioList);
      dbHelper.insertTestInvoicesIntoDb(portfolioList);
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @Test
  public void testApp() {
    try {
      MessageProducer jmsProducer = activeMQ.getActiveMqSession().createProducer(activeMqHelper.getDestinationAll());
      jmsProducer.setDeliveryMode(DeliveryMode.NON_PERSISTENT);
      MessageConsumer jmsConsumer = activeMQ.getActiveMqSession().createConsumer(activeMqHelper.getDestinationProcessed());

      Portfolio testPortfolio = portfolioList.get(0);
      TextMessage inputMessage = activeMqHelper.createTextMessage(testPortfolio);
      jmsProducer.send(inputMessage);

      /*waiting for the message in Processed queue*/
//      activeMqHelper.waitMessage(jmsConsumer);
      Assert.assertTrue(activeMqHelper.waitPortfolioMessage(jmsConsumer, testPortfolio));

      /*Check invoice level changed in DB*/
      int testInvoiceLevel = dbHelper.getTestInvoiceLevel(testPortfolio.invoices.get(0));
      Assert.assertEquals(InvoiceLevel.FUNDED.getValue(),testInvoiceLevel);

      /*Check portfolio status in DB*/
      int testPortfolio1Status = dbHelper.getTestPortfolioStatus(testPortfolio);
      Assert.assertEquals(PortfolioStatus.READY.getValue(), testPortfolio1Status);

    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  @After
  public void close() {
    dbHelper.deleteTestInvoices(portfolioList);
    activeMQ.close();
  }
}
